var students = ['Алексей Петров', 0,'Ирина Овчинникова', 60,'Глеб Стукалов', 30,'Антон Павлович', 30,'Виктория Заровская', 30,'Алексей Левенец', 70,'Тимур Вамуш', 30,'Евгений Прочан', 60,'Александр Малов', 0];
var studentPoints = 0;

console.log('Список студентов:');
for (var i = 0; i < students.length; i = i + 2) {
  console.log('Студент ' + students[i] + ' набрал ' + students[i+1] + ' баллов');
  if (students[studentPoints + 1] < students[i + 1]) {
    studentPoints = i;
  }
}
console.log('Студент, набравший максимальный балл:');
console.log('Студент ' + students[studentPoints] + ' имеет максимальный балл ' + students[studentPoints + 1]);

students.push('Николай Фролов', 0, 'Олег Боровой', 0);

// Исправил условие
var target1 = 'Антон Павлович';
var target2 = 'Николай Фролов';
// for (var i = 0; i < students.length; i = i + 2) {
//     var student = students[i];
//     if (student.indexOf(target1) == 0 || student.indexOf(target2) == 0) {
//       students[i + 1] += 10;
//   }
// }
students[students.indexOf(target1) + 1] += 10;
students[students.indexOf(target2) + 1] += 10;

console.log('Студенты, не набравшие баллов:');
for (var i = 0; i < students.length; i = i + 2) {
    if (students[i + 1] == 0) {
      console.log(students[i]);
      students.splice(i, 2);
      if (i != students.length) {
        i = i - 2;
      }
  }
}
